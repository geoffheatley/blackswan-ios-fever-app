//
//  CascadeLayout.h
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import <UIKit/UIKit.h>

#pragma mark - Constants

extern NSString *const CascadeElementKindSectionHeader;
extern NSString *const CascadeElementKindSectionFooter;

#pragma mark - CascadeLayout

@class CascadeLayout;

@protocol CascadeLayout <UICollectionViewDelegate>

@required

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath;

@optional

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section;
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section;


@end

#pragma mark - CascadeLayout

@interface CascadeLayout : UICollectionViewLayout

@property (nonatomic, assign) NSInteger columnCount;
@property (nonatomic, assign) CGFloat itemWidth;
@property (nonatomic, assign) CGFloat headerHeight;
@property (nonatomic, assign) CGFloat footerHeight;
@property (nonatomic, assign) UIEdgeInsets sectionInset;
@property (nonatomic, assign) CGFloat verticalItemSpacing;

@end

