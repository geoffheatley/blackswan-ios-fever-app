//
//  CascadeCell.h
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class FlickrPhoto;

@interface CascadeCell : UICollectionViewCell

@property (nonatomic, copy) NSString *displayString;
@property (nonatomic, strong) IBOutlet UILabel *displayLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) FlickrPhoto *photo;

@end
