//
//  CascadeFooter.m
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import "CascadeFooter.h"

@implementation CascadeFooter

#pragma mark - Accessors

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor redColor];
        
    }
    
    return self;
}

@end
