//
//  CascadeHeader.m
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import "CascadeHeader.h"

@implementation CascadeHeader

#pragma mark - Accessors

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor redColor];
        
    }
    
    return self;
}

@end
