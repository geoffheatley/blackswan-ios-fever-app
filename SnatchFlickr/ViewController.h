//
//  ViewController.h
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import <UIKit/UIKit.h>
#import "CascadeLayout.h"

@interface ViewController : UIViewController <UICollectionViewDataSource, CascadeLayout>

@property(nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic) CGFloat cellWidth;

@end

