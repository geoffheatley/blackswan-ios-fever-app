//
//  AppDelegate.h
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

