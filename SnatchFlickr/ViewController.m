//
//  ViewController.m
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import "ViewController.h"
#import "Flickr.h"
#import "FlickrPhoto.h"

#import "CascadeCell.h"
#import "CascadeHeader.h"
#import "CascadeFooter.h"
#import "CascadeLayout.h"

#define CELL_WIDTH 245
#define CELL_COUNT 20
#define CELL_IDENTIFIER @"CascadeCell"
#define HEADER_IDENTIFIER @"CascadeHeader"
#define FOOTER_IDENTIFIER @"CascadeFooter"


@interface ViewController () <UITextFieldDelegate>

@property(nonatomic, weak) IBOutlet UIToolbar* toolbar;
@property(nonatomic, weak) IBOutlet UIBarButtonItem* shareButton;
@property(nonatomic, weak) IBOutlet UITextField* textField;
@property(nonatomic, weak) IBOutlet UIView* containerView;

@property (nonatomic, strong) NSMutableArray* cellHeights;
@property (nonatomic, strong) NSMutableArray* imageUrls;
@property (nonatomic, strong) NSArray* sizesArray;


@property(nonatomic, strong) NSMutableDictionary* searchResults;
@property(nonatomic, strong) NSMutableArray* searches;
@property(nonatomic, strong) Flickr* flickr;


@end



@implementation ViewController


#pragma mark - Life Cycle


- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        
        self.cellWidth = CELL_WIDTH;
        
        
    }
    
    return self;
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    self.view.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    
    UIImage *textFieldImage = [[UIImage imageNamed:@"search_field.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [self.textField setBackground:textFieldImage];
    
    self.searches = [@[] mutableCopy];
    self.searchResults = [@{} mutableCopy];
    self.flickr = [[Flickr alloc] init];
    
    [self textFieldShouldReturn:self.textField];
    
    [self.containerView addSubview:self.collectionView];
    
    self.sizesArray = [NSArray arrayWithObjects:[NSNumber numberWithUnsignedInteger:350], [NSNumber numberWithUnsignedInteger:CELL_WIDTH], [NSNumber numberWithUnsignedInteger:100], nil];
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self updateLayout];
    
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    CascadeLayout* cl = (CascadeLayout*)self.collectionView.collectionViewLayout;
    
    switch (toInterfaceOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            //self.cellWidth = 124;
            cl.columnCount = 3;
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            
            cl.columnCount = 4;
            
            break;
            
        default:
            break;
    }
    
    [self updateLayout];
    
}


- (void)updateLayout {
    
    CascadeLayout* layout = (CascadeLayout*)self.collectionView.collectionViewLayout;
    //layout.columnCount = self.collectionView.bounds.size.width / self.cellWidth;
    layout.itemWidth = self.cellWidth;
    
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}


#pragma mark - Accessors


- (UICollectionView *)collectionView {
    
    if (!_collectionView) {
        
        CascadeLayout* layout = [[CascadeLayout alloc] init];
        
        layout.sectionInset = UIEdgeInsetsMake(9, 9, 9, 9);
        layout.verticalItemSpacing = 7;
        layout.headerHeight = 15;
        layout.footerHeight = 10;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.containerView.bounds collectionViewLayout:layout];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        
        [_collectionView registerClass:[CascadeCell   class] forCellWithReuseIdentifier:CELL_IDENTIFIER];
        [_collectionView registerClass:[CascadeHeader class] forSupplementaryViewOfKind:CascadeElementKindSectionHeader withReuseIdentifier:HEADER_IDENTIFIER];
        [_collectionView registerClass:[CascadeFooter class] forSupplementaryViewOfKind:CascadeElementKindSectionFooter withReuseIdentifier:FOOTER_IDENTIFIER];
        
    }
    
    return _collectionView;
    
}



- (NSMutableArray *)cellHeights {
    
    if (!_cellHeights) {
        
        _cellHeights = [NSMutableArray arrayWithCapacity:CELL_COUNT];
        
        for (NSInteger i = 0; i < CELL_COUNT; i++) {
            
            //_cellHeights[i] = /*@320;*/ @(arc4random() % 100 * 2 + 100);
            
            NSUInteger randomIndex = arc4random() % [_sizesArray count];
            _cellHeights[i] = [_sizesArray objectAtIndex:randomIndex];
            
        }
        
    }
    
    return _cellHeights;
    
}



#pragma mark - UITextFieldDelegate methods


- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    
    [self.flickr searchFlickrForTerm:textField.text completionBlock:^(NSString *searchTerm, NSArray *results, NSError *error) {
        
        NSLog(@"array_results:%@", results);
        
        if (results && [results count] > 0) {
            
            if (![self.searches containsObject:searchTerm]) {
                
                NSLog(@"Found %lu photos matching %@", (unsigned long)[results count],searchTerm);
                [self.searches insertObject:searchTerm atIndex:0];
                //[self.searches replaceObjectAtIndex:0 withObject:searchTerm];
                self.searchResults[searchTerm] = results;
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[self.collectionView reloadData];
                [self updateLayout];
                [_collectionView reloadData];
                
            });
            
        } else {
            
            NSLog(@"Error searching Flickr: %@", error.localizedDescription);
            
        } }];
    
    [textField resignFirstResponder];
    return YES;
    
}



#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return (self.searches.count == 0) ? 0 : CELL_COUNT;
    
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    
    return 1;
    
}


- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath {
    
    CascadeCell* cell = (CascadeCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    NSString* searchTerm = self.searches[0];
    cell.photo = self.searchResults[searchTerm][indexPath.item];
    
    //cell.imageView.image = [UIImage imageNamed:@"southamptonSpitfires.png"];    //[self.imageUrls objectAtIndex:indexPath.item % 5]];
    
    cell.displayString = [NSString stringWithFormat:@"%ld", (long)indexPath.item];
    
    return cell;
    
}


- (UICollectionReusableView *)collectionView:(UICollectionView*)collectionView viewForSupplementaryElementOfKind:(NSString*)kind atIndexPath:(NSIndexPath*)indexPath {
    
    UICollectionReusableView* reusableView = nil;
    
    if ([kind isEqualToString:CascadeElementKindSectionHeader]) {
        
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:HEADER_IDENTIFIER forIndexPath:indexPath];
        
    } else if ([kind isEqualToString:CascadeElementKindSectionFooter]) {
        
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:FOOTER_IDENTIFIER forIndexPath:indexPath];
    }
    
    return reusableView;
    
}




#pragma mark - CascadeLayoutDelegate


- (CGFloat)collectionView:(UICollectionView*)collectionView layout:(CascadeLayout*)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath*)indexPath {
    
    //return [self.cellHeights[indexPath.item] floatValue];
    NSString* searchTerm = self.searches[0];
    
    FlickrPhoto* photo = self.searchResults[searchTerm][indexPath.item];
    
    float ratio = photo.thumbnail.size.height / photo.thumbnail.size.width;
    
    return CELL_WIDTH * ratio;
    
}





@end
