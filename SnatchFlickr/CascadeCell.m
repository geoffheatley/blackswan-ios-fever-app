//
//  CascadeCell.m
//  SnatchFlickr
//
//  Created by geoffheatley on 2/22/16.
//
//

#import "CascadeCell.h"
#import "FlickrPhoto.h"

@implementation CascadeCell

#pragma mark - Accessors

- (UILabel*)displayLabel {
    
    if (!_displayLabel) {
        
        _displayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.imageView.frame.size.width/10, self.imageView.frame.size.width/10)];
        _displayLabel.frame = CGRectMake(self.imageView.frame.size.width/20, self.imageView.frame.size.width/20, _displayLabel.frame.size.width, _displayLabel.frame.size.height);
        _displayLabel.textColor = [UIColor whiteColor];
        _displayLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    
    return _displayLabel;
    
}



- (UIImageView*)imageView {
    
    if (!_imageView) {
        
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
    }
    
    return _imageView;
}


- (void)setDisplayString:(NSString *)displayString {
    
    if (![_displayString isEqualToString:displayString]) {
        
        _displayString = [displayString copy];
        self.displayLabel.text = _displayString;
        
    }
    
}

#pragma mark - Life Cycle

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.displayLabel];
        self.contentView.backgroundColor = [UIColor lightGrayColor];
        
    }
    
    return self;
    
}


- (void)setPhoto:(FlickrPhoto*)photo {
    
    if (_photo != photo) _photo = photo;
    self.imageView.image = _photo.thumbnail;
    
}

@end
